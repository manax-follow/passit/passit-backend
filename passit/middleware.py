from server_side_piwik.middleware import PiwikMiddleware

class PassitPiwikMiddleware(PiwikMiddleware):
    def __call__(self, request):
        """ Implements a url blacklist before running piwik middleware """
        path = request.get_full_path()
        blacklist = (
            '/api/user-public-auth/',
            '/api/ping/',
        )
        if path.startswith(blacklist):
            return self.get_response(request)
        return super().__call__(request)

